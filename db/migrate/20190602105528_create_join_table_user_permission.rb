class CreateJoinTableUserPermission < ActiveRecord::Migration[5.0]
  def change
    create_join_table :users, :components do |t|
      # t.index [:user_id, :component_id]
      # t.index [:component_id, :user_id]
    end
  end
end
