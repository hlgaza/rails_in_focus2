class CreateJoinTableRolePermission < ActiveRecord::Migration[5.0]
  def change
    create_join_table :roles, :components do |t|
      # t.index [:role_id, :component_id]
      # t.index [:component_id, :role_id]
    end
  end
end
