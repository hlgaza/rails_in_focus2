class User < ApplicationRecord
    validates :username, presence: true

    has_and_belongs_to_many :roles, :join_table => "roles_users"
    has_and_belongs_to_many :permissions, :join_table => "components_users", :class_name => "Component"

    def can_access?(component)
        permissions.include?(component) && user_role_permissions_include?(component)
    end

    def user_role_permissions_include?(component)
        roles.joins(:permissions).where("components.id = #{component.id}").any?
    end
end
