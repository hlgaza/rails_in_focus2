class Component < ApplicationRecord
    validates :name, presence: true
end
