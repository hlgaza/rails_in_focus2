class Role < ApplicationRecord
    validates :name, presence: true

    has_and_belongs_to_many :users, :join_table => "roles_users"
    has_and_belongs_to_many :permissions, :join_table => "components_roles", :class_name => "Component"
end